package probno;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        final long startTime1 = System.currentTimeMillis();
        for (int i = 0; i <= 10000000; i++) {
            Person p1 = new Person(new Date());
            p1.isBabyBoomer();
        }
        final long endTime1 = System.currentTimeMillis();

        System.out.println("Total execution time: " + (endTime1 - startTime1));

        final long startTime2 = System.currentTimeMillis();
        for (int i = 0; i <= 10000000; i++) {
            ImprovedPerson p2 = new ImprovedPerson(new Date());
            p2.isBabyBoomer();
        }
        final long endTime2 = System.currentTimeMillis();

        System.out.println("Total execution time: " + (endTime2 - startTime2));
    }

}
